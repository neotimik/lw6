package com.human.lw15;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
ExpandableListView expandableListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        expandableListView = (ExpandableListView) findViewById(R.id.elv);

        Map<String, String> map;
        ArrayList<Map<String, String>> groupDataList = new ArrayList<>();

        map = new HashMap<>();
        map.put("groupName", getResources().getString(R.string.pizzaName));
        groupDataList.add(map);

        String groupFrom[] = new String[] { "groupName" };
        int groupTo[] = new int[] { android.R.id.text1 };

        ArrayList<ArrayList<Map<String, String>>> childDataList = new ArrayList<>();
        ArrayList<Map<String, String>> childDataItemList = new ArrayList<>();

        String[] ingredients = getResources().getStringArray(R.array.ingredients);
        for(String item: ingredients) {
            map = new HashMap<>();
            map.put("ingredients", item);
            childDataItemList.add(map);
        }
        childDataList.add(childDataItemList);
        String childFrom[] = new String[] { "ingredients" };
        int childTo[] = new int[] { android.R.id.text1 };

        SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(
                this, groupDataList, android.R.layout.simple_expandable_list_item_1,
                groupFrom, groupTo,childDataList,android.R.layout.simple_list_item_1,
                childFrom, childTo
        );
        expandableListView.setAdapter(adapter);
    }
}
